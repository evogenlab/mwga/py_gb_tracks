# Python GenomeBrowser tracks library

Python library to generate GenomeBrowser track files.

Currently implemented:
- Creating track hubs
- Wig to bed conversion

## Installation

```bash
pip install py-GB-tracks
```
