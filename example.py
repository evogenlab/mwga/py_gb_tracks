import logging
from py_GB_tracks import TrackHub
from py_GB_tracks.utilities import create_default_hub

wig_file_path = 'example/example.wig'
gtf_file_path = 'example/example.gtf'
bed_file_path = 'example/example.bed'

wig_file_path = '/home/romain/work/analyses/wga/results/phastCons.wig'
gtf_file_path = '/home/romain/work/analyses/wga/data/Anopheles-gambiae-PEST_BASEFEATURES_AgamP4.11.gtf'
bed_file_path = '/home/romain/work/analyses/wga/results/phastCons_exons_test.bed'

converter = WigToBed(log_level=logging.INFO, colors=((0, 0, 255), (255, 0, 0)))
# converter.wig_to_bed(wig_file_path, gtf_file_path)
converter.wig_to_bed(wig_file_path, gtf_file_path, bed_file_path=bed_file_path)

genomes = [{'genome': 'AgamP4',
            'organism': 'Anopheles gambiae PEST strain',
            'defaultPos': '2L:0-100000',
            'orderKey': '1000',
            'scientificName': 'Anopheles gambiae PEST strain'}]

hub = create_default_hub(root_dir='track2', hub_name='AgamP4', email='romain.feron@unil.ch', genomes=genomes)

hub.add_track('AgamP4', 'bam', track_data={'track': 'test',
                                           'superTrack': 'on show',
                                           'shortLabel': 'testshortLabel',
                                           'longLabel': 'testlongLabel',
                                           'bigDataUrl': 'testbigDataUrl',
                                           'bamColorMode': 'bamColorModeTest',
                                           'priority': 1})

hub.add_track('AgamP4', 'bam', track_data={'track': 'test2',
                                           'shortLabel': 'testshortLabel2',
                                           'longLabel': 'testlongLabel2',
                                           'bigDataUrl': 'testbigDataUrl2',
                                           'bamColorMode': 'bamColorModeTest',
                                           'parent': 'test',
                                           'priority': 2})

hub.add_track('AgamP4', 'bam', track_data={'track': 'test4',
                                           'shortLabel': 'testshortLabel2',
                                           'longLabel': 'testlongLabel2',
                                           'bigDataUrl': 'testbigDataUrl2',
                                           'bamColorMode': 'bamColorModeTest',
                                           'parent': 'test',
                                           'priority': 1})

hub.add_track('AgamP4', 'bam', track_data={'track': 'test3',
                                           'shortLabel': 'testshortLabel',
                                           'longLabel': 'testlongLabel',
                                           'bigDataUrl': 'testbigDataUrl',
                                           'bamColorMode': 'bamColorModeTest',
                                           'priority': 3})

hub.add_track('AgamP4', 'bam', track_data={'track': 'test5',
                                           'shortLabel': 'testshortLabel',
                                           'longLabel': 'testlongLabel',
                                           'bigDataUrl': 'testbigDataUrl',
                                           'bamColorMode': 'bamColorModeTest',
                                           'priority': 2})

hub.write_tracks()

# hub = TrackHub.load('track2')
# print(hub.genome_file.genomes)
# print(hub.hub_file.hub)
# print(hub.hub_file.shortLabel)

# ofile = open('track2/test.txt', 'w')
# hub.tracks[0].write_to_file(ofile)
